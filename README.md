# High Gas Agent

## Description

This agent detects manipulation across governence systems for uniswap

## Supported Chains

- Ethereum

## Alerts

Describe each of the type of alerts fired by this agent

- UNISWAP-1
  - Fired when a transaction includes votes which were manipulated 100 blocks leading up to to the vote cast.
  - Severity is always set to "high" 
  - Type is always set to "suspicious" 
- UNISWAP-2
  - Fired when a transaction includes votes which decreased within 100 blocks after the vote was casted..
  - Severity is always set to "high" 
  - Type is always set to "suspicious" 

