const { Finding, FindingSeverity, FindingType, getEthersProvider, ethers, BigNumber} = require("forta-agent");
const {handleNewVotes} = require("./new.votes.checker")
const {checkOldVotes} = require("./old.votes.checker");
const {
  UNISWAP_ABI,
  UNISWAP_ADDRESS,
  GOV_ADDRESS,
  GOV_ABI
} = require("./vars");

const contracts = {
  UNISWAP_CONTRACT: new ethers.Contract(UNISWAP_ADDRESS, UNISWAP_ABI, getEthersProvider()),
  GOV_CONTRACT: new ethers.Contract(GOV_ADDRESS, GOV_ABI, getEthersProvider())
};

const voters = [];
/*
{
  (voter, votes, txEvent.receipt.blockNumber);
}
*/

function provideHandleTransaction(contracts, voters) {
  return async function (txEvent) {
    const findings = await handleNewVotes(txEvent, contracts, voters);
    return findings;
  }
}

function provideHandleBlock(contracts, voters) {
  return async function (blockEvent) {
    const findings = await checkOldVotes(blockEvent, contracts, voters);
    return findings;
  }
}


module.exports = {
  provideHandleTransaction,
  provideHandleBlock,
  handleTransaction: provideHandleTransaction(contracts, voters),
  handleBlock: provideHandleBlock(contracts, voters)
};